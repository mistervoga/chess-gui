from flask import Flask, render_template, request
from flask_cors import CORS
import chess
import chess.pgn
import chess.engine

app = Flask(__name__)
CORS(app)

engine = chess.engine.SimpleEngine.popen_uci("/usr/bin/stockfish")

game = chess.pgn.Game()


@app.route('/')
def index():
    board = chess.Board()
    result = None
    pgn = ""
    return render_template('index.html', board=board, result=result, pgn=pgn)


@app.route('/make_move', methods=['POST'])
def make_move():
    global game
    source = request.form['source']
    target = request.form['target']
    board_fen = request.form['fen']
    board = chess.Board(board_fen)

  # Check if castling move
    if board.is_castling(chess.Move.from_uci(source + target)):
        # Get the king and rook squares for castling
        if target == 'g1':
            king_square, rook_square, king_dest, rook_dest = chess.E1, chess.H1, chess.G1, chess.F1
        elif target == 'c1':
            king_square, rook_square, king_dest, rook_dest = chess.E1, chess.A1, chess.C1, chess.D1
        elif target == 'g8':
            king_square, rook_square, king_dest, rook_dest = chess.E8, chess.H8, chess.G8, chess.F8
        elif target == 'c8':
            king_square, rook_square, king_dest, rook_dest = chess.E8, chess.A8, chess.C8, chess.D8

        # Update FEN string with new castling rights and king and rook positions
        if target in ['g1', 'g8']:
            board.set_castling_fen(board.castling_rights,
                                   king_dest, rook_dest, '-')

        elif target in ['c1', 'c8']:
            board.set_castling_fen(board.castling_rights,
                                   king_dest, rook_dest, '')

    else:
        # Push the move onto the board
        move = chess.Move.from_uci(source + target)
        if move not in board.legal_moves:
            raise chess.engine.InvalidMoveError(move)

        if board.is_capture(move):
            # Remove the captured piece from the board
            board.remove_piece_at(chess.SQUARES[chess.parse_square(target)])

        board.push(move)

    # Make engine move
    result = engine.play(board, chess.engine.Limit(time=2.0))
    board.push(result.move)

    # Add current move to game object
    node = game.add_variation(chess.Move.from_uci(source + target))
    # Add engine move to game object
    node = node.add_variation(result.move)

    # Get PGN from move history
    pgn = get_pgn(game)

    # Return FEN string, PGN, and move history of board
    return {'fen': board.fen(), 'pgn': pgn}


def get_pgn(game):
    return str(game)


if __name__ == '__main__':
    app.run()
